import logo from './logo.svg';
import './App.css';
import { Root } from './components/Root';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Root />  
      </header>
    </div>
  );
}

export default App;
